<?php
function is_status_completed( $order_id ) {
	if ( ! $order_id ) {
		return;
	}

	$order      = wc_get_order( $order_id );
	$order_data = $order->get_data();
	$items      = $order->get_items();

	$product_id     = "";
	$product_name   = "";
	$parent_id      = "";

	// prepare data user
	$an_id             = $order_id;
	$an_email          = $order_data['billing']['email'];
	$an_firstname      = $order_data['billing']['first_name'];
	$an_lastname       = $order_data['billing']['last_name'];
	$an_phone          = $order_data['billing']['phone'];
	$product_info       = [];

	// get product id and name loop
	foreach ( $items as $item ) {
		$product_cat_slug = "";
		$workshop = false;
		$coaching = false;

		$product_name   = $item->get_name();
		$product_id     = $item->get_product_id();
		$an_product_link= get_the_permalink($product_id).'#button-review';
		$terms          = get_the_terms( $product_id, 'product_cat' );

		$info   = [
			"product_name"  => $product_name,
			"product_link"  => $an_product_link
		];
		array_push($product_info, $info);
	}

	$an_data = [
		"id"            => $an_id,
		"email"         => $an_email,
		"firstName"     => $an_firstname,
		"lastName"      => $an_lastname,
		"phone"         => $an_phone,
		"product_info"  => $product_info,
	];

//	echo '<pre>';
//	print_r($an_data);
//	echo '</pre>';

	$handlerMail = new handlerEmail;
	//send mail to customer
	$handlerMail->mailto_fill_review($an_data);
	echo "email sent";
	// end get product id and name loop
	//Do whatever additional logic you like before….
	return 'completed';
}

// add the action
add_action( 'woocommerce_order_status_completed', 'is_status_completed', 10, 1 );