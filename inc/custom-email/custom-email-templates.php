<?php
class handlerEmail {
	// Email send link review
	public function mailto_fill_review($data){
		global $woocommerce;
		// Create a mailer
		$mailer = $woocommerce->mailer();
		$message_body  = __( "<p><strong>Beste ".$data['firstName'].",</strong></p>");
		$message_body .= __( "<br/>");
		$message_body .= __( "<p>Onlangs hebben we met veel plezier de :");
		foreach ($data['product_info'] as $key => $k) {
		$message_body .= __( "<br/>");
		$message_body .= __( "<strong>".$k['product_name']."</strong>");
		}
		$message_body .= __( "<br/>");
		$message_body .= __( " bij u afgeleverd. Wij zijn een startende onderneming, en het zou ons enorm helpen als u een beoordeling achterlaat over uw product. Hierdoor zien klanten dat het product bevalt en groeien wij als nieuw merk. Het lijkt een kleine moeite maar het is een wereld van verschil voor ons.</p>");
		$message_body .= __( "<br/><br/>");
		$message_body .= __( "<p>");
		$message_body .= __( "Een review maken duurt niet lang, dat beloven we.");
		$message_body .= __( "</p>");
		$message_body .= __( "<br/><br/>");
		foreach ($data['product_info'] as $key => $d) {
		$message_body .= __( "<p>button write review <strong>schrijf review </strong> <a href='".$d['product_link']."' target='_blank'><strong>".$d['product_name']."</strong></a></p>");
		$message_body .= __( "<br/><br/>");
		}
		$message_body .= __( "<br/><br/>");
		$message_body .= __( "<br/><p>Hartelijk bedankt voor uw hulp! Wij waarderen het enorm. Als u enige vragen heeft, dan staan wij klaar!</p>");
		$message_body .= __( "<br/><br/>");
		$message_body .= __( "<p>Met hartelijke groeten,</p>");
		$message_body .= __( "<br/><br/><br/><br/>");
		$message_body .= __( "<p>Team Litollo</p>");
		$message = $mailer->wrap_message(
		// Message head and message body.
			"Litollo - Recensie", $message_body);
		// Cliente email, email subject and message.
		$mailer->send( $data['email'], "Litollo - Recensie", $message );
	}
}
new handlerEmail();