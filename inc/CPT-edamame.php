<?php
// Register Custom edamame
function job_post_type() {

	$labels = array(
		'name'                  => _x( 'edamames', 'edamame General Name', 'wappbs4' ),
		'singular_name'         => _x( 'edamame', 'edamame Singular Name', 'wappbs4' ),
		'menu_name'             => __( 'edamames', 'wappbs4' ),
		'name_admin_bar'        => __( 'edamame', 'wappbs4' ),
		'archives'              => __( 'Job Archives', 'wappbs4' ),
		'attributes'            => __( 'Job Attributes', 'wappbs4' ),
		'parent_item_colon'     => __( 'Parent Job:', 'wappbs4' ),
		'all_items'             => __( 'All Jobs', 'wappbs4' ),
		'add_new_item'          => __( 'Add New Job', 'wappbs4' ),
		'add_new'               => __( 'Add New', 'wappbs4' ),
		'new_item'              => __( 'New Job', 'wappbs4' ),
		'edit_item'             => __( 'Edit Job', 'wappbs4' ),
		'update_item'           => __( 'Update Job', 'wappbs4' ),
		'view_item'             => __( 'View Job', 'wappbs4' ),
		'view_items'            => __( 'View Jobs', 'wappbs4' ),
		'search_items'          => __( 'Search Job', 'wappbs4' ),
		'not_found'             => __( 'Not found', 'wappbs4' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wappbs4' ),
		'featured_image'        => __( 'Featured Image', 'wappbs4' ),
		'set_featured_image'    => __( 'Set featured image', 'wappbs4' ),
		'remove_featured_image' => __( 'Remove featured image', 'wappbs4' ),
		'use_featured_image'    => __( 'Use as featured image', 'wappbs4' ),
		'insert_into_item'      => __( 'Insert into item', 'wappbs4' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'wappbs4' ),
		'items_list'            => __( 'Jobs list', 'wappbs4' ),
		'items_list_navigation' => __( 'Jobs list navigation', 'wappbs4' ),
		'filter_items_list'     => __( 'Filter items list', 'wappbs4' ),
	);
	$args = array(
		'label'                 => __( 'edamame', 'wappbs4' ),
		'description'           => __( 'edamames', 'wappbs4' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ), //if able the content: add editor, author, post-formats
		'taxonomies'            => array(),
		'hierarchical'          => false, // to disable the archive
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
    'rewrite'               => array('slug' => __( 'jobs', 'wappbs4' )),
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'job_post_type', $args );

}
add_action( 'init', 'job_post_type', 0 );
