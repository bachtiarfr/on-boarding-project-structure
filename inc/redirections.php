<?php
/**
 *
 * Register Redirections Link
 *
 */

//add_action('wp_print_styles', 'redirect_old_url_to_new_url');
//function redirect_old_url_to_new_url()
//{
//	global $post;
//	// if ('shop' == $post->post_name) {
//	if (is_shop() || is_product_category()) {
//		wp_redirect(site_url() . '/zwangerschap', 301);
//		die();
//	}
//}

// redirect after logout
//add_action('wp_logout','auto_redirect_after_logout');
//function auto_redirect_after_logout(){
//  wp_redirect( home_url() );
//  exit();
//}

//add_filter( 'login_redirect', 'redirect_admin', 10, 3 );
//function redirect_admin( $redirect_to, $request, $user ){
//    $referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";
//    //is there a user to check?
//    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
//
//        //check for admins
//        if ( !in_array( 'administrator', $user->roles ) ) {
//            if (strstr($referrer, 'checkout-login/')){
//                $redirect_to = wc_get_checkout_url();
//            }else{
//                $redirect_to = site_url(); // Your redirect URL
//            }
//
//        }else{
//            $redirect_to = admin_url();
//        }
//    }
//
//    return $redirect_to;
//}


// cegatan checkout
//add_action('template_redirect', 'redirect_cegatan_checkout');
//function redirect_cegatan_checkout()
//{
//	if (is_checkout() && !is_user_logged_in() && !$_GET['user'] == "guest") {
//        wp_redirect(site_url() . '/checkout-login', 301);
//		die();
//	}
//}

// redirect forgot password
//add_action( 'login_form_lostpassword','redirect_to_custom_lostpassword' );
//function redirect_to_custom_lostpassword() {
//	if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
//		if ( is_user_logged_in() ) {
//			$this->redirect_logged_in_user();
//			exit;
//		}
//		wp_redirect( site_url( 'reset-password' ) );
//		exit;
//	}
//}