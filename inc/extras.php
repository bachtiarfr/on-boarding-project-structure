<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package cones
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function cones_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'cones_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function cones_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'cones_pingback_header' );


/**
 * Allow .svg upload
 *
 * To use this function works, add this code in your wp-config.php: define('ALLOW_UNFILTERED_UPLOADS', true);
 */
function upload_my_images($mimes)
{
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'upload_my_images');

/*
 * Custom menu output
 * No <ul>, no <li>, just <a>
 * headlab_menu('location')
 **/
function headlab_menu($location)
{
	// Get our nav locations (set in our theme, usually functions.php)
	$menuLocations = get_nav_menu_locations(); // This returns an array of menu locations;
	$menuID = $menuLocations[$location]; // Get the *MENU* menu ID
	$menu_navs = wp_get_nav_menu_items($menuID);
	$queried_page_id = (get_queried_object_id()) ? get_queried_object_id() : woocommerce_get_page_id('shop');

	//var_dump($queried_page_id);
	foreach ($menu_navs as $menu_nav) {
		$object_id = $menu_nav->object_id;
		$hasParent = intval($menu_nav->menu_item_parent);

		if ($queried_page_id == $object_id) {
			$active = " class='active main-item'";
		} else {
			$active =  " class='main-item'";
		}
		if(!$hasParent) {
			echo '<a href="' . esc_url($menu_nav->url) . '" ' . $active . '>' . esc_html($menu_nav->title) . '</a>';
		}
	}
}

// add_action( 'wp', 'ts_remove_zoom_lightbox_gallery_support', 99 );

// function ts_remove_zoom_lightbox_gallery_support() { 
//   remove_theme_support( 'wc-product-gallery-zoom' );
//   remove_theme_support( 'wc-product-gallery-lightbox' );
//   remove_theme_support( 'wc-product-gallery-slider' );
// }


add_filter( 'wc_add_to_cart_message_html', function ($message, $products){
	foreach( $products as $product_id => $val ) {
		// (If needed) get the WC_Product object
		$product = wc_get_product($product_id);
		// The product title
		$product_title = $product->get_title();
	}
	//get data review
	$revs = [];
	$queryOpts = array(
		'nopaging' => true,
		'post_type' => 'wpcr3_review',
		'post_status' => 'publish'
	);
	$posts = new \WP_Query($queryOpts);

	foreach ($posts->posts as $key => $post) {
		$rvRating   = get_post_meta($post->ID, 'wpcr3_review_rating' ,true);
		$reviewed_post_id = get_post_meta($post->ID, 'wpcr3_review_post', true);
		$rev = [
			'id' => $reviewed_post_id,
			'rate'  => $rvRating
		];
		array_push($revs, $rev);
	}
	//end get data review

	$star = 0;
	$c = 0;

	foreach ($revs as $key => $rev) {
		if($rev['id'] == $product_id){
			$c++;
			$star += (int)$rev['rate'];
		}
	}

	if($c !== 0){
		$stars = $star / $c;
	}else{
		$stars = 5;
	}
	
	$args = [
		'post_type' => ['product_variation'],
		'orderby' => 'meta_value_num',
		'order' => 'DESC',
		'post_status' => 'publish',
		'post_parent__in' => $has_variable_ids,
	];

	$var_query = new \WP_Query($args);

	?>
    <div class="modal fade single_add_to_cart_button" id="relatedProductModal" tabindex="-1"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <img src="<?= get_the_post_thumbnail_url($product_id); ?>" style="width: 150px;">
                            </div>
                            <div class="col-md-6 pl-0">
                                <div class="popup-star-rating">
									<?php
									if ($stars !== 0 && $stars !== null) {
										for ($i = 1; $i <= $stars; $i++) {
											?>
                                            <i class="fa fa-star"></i>
											<?php
										}
									} else {
										?>
                                        <div class="star-ratings">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
										<?php
									}
									?>
                                    <h5 class="modal-title pl-0" id="exampleModalLabel"><?= $product_title; ?></h5>
                                </div>
                            </div>
                            <div class="col-md-3 pr-0 cta-top">
                                <div class="row d-flex justify-content-center">
                                    <a href="<?= wc_get_cart_url(); ?>" class="btn btn-primary">Nu Afrekenen</a>
                                </div>
                                <div class="row d-flex justify-content-center">
                                    <span>Of <a href="#" class="benelux-link">Litollo Benelux</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container product-variation">
                        <div class="related-popup-title">
                            <h2>Wij raden u deze producten aan</h2>
                        </div>
                        <div class="row slickBaru">
							<?php

							if (have_rows('pop_up_products', $product_id)) :
								while (have_rows('pop_up_products', $product_id)) : the_row();

									$pID = get_sub_field('select_products', $product_id);
									$product = wc_get_product($pID);
									?>
                                    <div class="col-md-4">
                                        <a href="<?php echo esc_url(the_permalink($pID)); ?>">
                                            <div>
												<?php echo do_shortcode('[add_to_cart id=' . $pID . ' show_price="false"]') ?>
                                            </div>
                                            <img class="related-popup-image" src="<?= get_the_post_thumbnail_url($pID); ?>">

                                            <h6 class="variation-title"> <?= get_the_title($pID); ?> </h6>
                                            <div class="variation-product">
												<?= $product->get_price_html() ?>
                                            </div>
											<?php
											?>
                                        </a>
                                    </div>
								<?php
								endwhile;
                            else:
	                            $args2 = array(
		                            'post_type'   => 'product',
		                            'post_status' => 'publish',
		                            'posts_per_page' => 3,
		                            'post__not_in'   => array( $product_id ),
		                            'orderby' => 'rand',
	                            );
	                            $var_query2 = new \WP_Query($args2);

	                            while ($var_query2->have_posts()) : $var_query2->the_post();
		                            $product = wc_get_product(get_the_ID()); ?>
                                    <div class="col-md-4">
                                        <a href="<?php echo esc_url(the_permalink()); ?>">
                                            <div>
					                            <?php echo do_shortcode( '[add_to_cart id=' . get_the_ID() . ' show_price="false"]' ) ?>
                                            </div>
                                            <img class="related-img" src="<?= get_the_post_thumbnail_url(get_the_ID()); ?>" >
                                            <h6 class="variation-title"> <?= get_the_title(); ?> </h6>
                                            <div class="variation-product">
					                            <?= $product->get_price_html() ?>
                                            </div>
				                            <?php
				                            ?>
                                        </a>
                                    </div>
	                            <?php
	                            endwhile;
							endif;
							?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-start">
                    <a href="<?= wc_get_cart_url(); ?>" class="btn btn-primary">Nu Afrekenen</a>
                    <span>Of <a href="#" class="benelux-link"> Litollo Benelux</a></span>
                </div>
            </div>
        </div>
    </div>
	<?php

	return $message;
}, 10, 2);

// move single variation add to cart button
remove_action('woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20); 
add_action('woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 5);

//email reset password
add_filter( 'retrieve_password_message', 'my_retrieve_password_message', 10, 4 );
function my_retrieve_password_message( $message, $key, $user_login, $user_data ) {

    $user_email = $user_data->data->user_email;
	global $woocommerce;
	$mailer = $woocommerce->mailer();
	// Start with the default content.
	$site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
	$message = __( 'Lemand heeft gevraagd om het wachtwoord opnieuw in te stellen voor het volgende account: ' ) . "<br/><br/>";
	/* translators: %s: site name */
	$message .= sprintf( __( 'Site Naam: %s' ), $site_name ) . "<br/>";
	/* translators: %s: user login */
	$message .= sprintf( __( 'Gebruikersnaam: %s' ), $user_login ) . "<br/><br/>";
	$message .= __( 'Als dit een vergissing was, negeer dan deze e-mail en er gebeurt niets.' ) . "<br/>";
	$message .= __( 'Ga naar de volgende link om uw wachtwoord opnieuw in te stellen:' ) . "<br/>";
//	$message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";

	/*
	 * If the problem persists with this filter, remove
	 * the last line above and use the line below by
	 * removing "//" (which comments it out) and hard
	 * coding the domain to your site, thus avoiding
	 * the network_site_url() function.
	 */
	 $message .= '<a href="http://dev-litollo.madeindonesia.com/wp-login.php?action=rp&key="' . $key . '&login=' . rawurlencode( $user_login ) .' ">';
	 $message .= 'http://dev-litollo.madeindonesia.com/wp-login.php?action=rp&key=' . $key . '&login=' . rawurlencode( $user_login );
	 $message .= '</a>';
//	 $message .= '<http://dev-litollo.test/wp-login.php?action=rp&key=' . $key . '&login=' . rawurlencode( $user_login ) . ">";

	$messageX = $mailer->wrap_message(
	// Message head and message body.
		"Wachtwoord reset", $message);
	// Cliente email, email subject and message.
	$mailer->send( $user_email, "Wachtwoord reset", $messageX );
	// Return the filtered message.
//	return true;

}

remove_filter( 'authenticate', 'wp_authenticate_email_password' );
add_filter( 'authenticate', 'custom_authenticate_email_password', 31, 3 );
function custom_authenticate_email_password( $user, $email, $password ) {
    if ($user instanceof WP_User) {
        return $user;
    }

    if (empty($email) || empty($password)) {
        $error = new WP_Error();
        if ( empty( $email ) ) {
            // Uses 'empty_username' for back-compat with wp_signon().
            $error->add( 'empty_username', __( 'De gebruikersnaam of het e-mailadres is leeg.' ) );
        }
        if ( empty( $password ) ) {
            $error->add( 'empty_password', __( 'Het wachtwoordveld is leeg.' ) );
        }
        return $error;
    }

    if (!is_email($email)) {
        return $user;
    }

    $user = get_user_by('email', $email);
    if (!$user) {
        return new WP_Error('invalid_email',__( 'Ongeldige gebruikersnaam of e-mailadres.' ));
    }

    /** This filter is documented in wp-includes/user.php */
    $user = apply_filters( 'wp_authenticate_user', $user, $password );
    if (is_wp_error($user)){
        return $user;
    }

    if (!wp_check_password( $password, $user->user_pass, $user->ID)){
        return new WP_Error('incorrect_password',sprintf( __( 'Het wachtwoord dat u heeft ingevoerd is onjuist.' ),
        $email, wp_lostpassword_url() ) );
    }

    return $user;
}


// move the variation price
// Utility function to get the default variation (if it exist)
function get_default_variation( $product ){
    $attributes_count = count($product->get_variation_attributes());
    $default_attributes = $product->get_default_attributes();
    // If no default variation exist we exit
    if( $attributes_count != count($default_attributes) )
        return false;

    // Loop through available variations
    foreach( $product->get_available_variations() as $variation ){
        $found = true;
        // Loop through variation attributes
        foreach( $variation['attributes'] as $key => $value ){
            $taxonomy = str_replace( 'attribute_', '', $key );
            // Searching for a matching variation as default
            if( isset($default_attributes[$taxonomy]) && $default_attributes[$taxonomy] != $value ){
                $found = false;
                break;
            }
        }
        // If we get the default variation
        if( $found ) {
            $default_variaton = $variation;
            break;
        }
        // If not we continue
        else {
            continue;
        }
    }
    return isset($default_variaton) ? $default_variaton : false;
}

add_action( 'woocommerce_before_single_product', 'move_variations_single_price', 1 );
function move_variations_single_price(){
    global $product, $post;

    if ( $product->is_type( 'variable' ) ) {
        // removing the variations price for variable products
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

        // Change location and inserting back the variations price
        add_action( 'woocommerce_single_product_summary', 'replace_variation_single_price', 10 );
    }
}

function replace_variation_single_price(){
    global $product;

    // Main Price
    $prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
    $active_price = $prices[0] !== $prices[1] ? sprintf( __( 'From: %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

    // Sale Price
    $prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
    sort( $prices );
    $regular_price = $prices[0] !== $prices[1] ? sprintf( __( 'From: %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

    if ( $active_price !== $regular_price && $product->is_on_sale() ) {
        $price = '<del>' . $regular_price . $product->get_price_suffix() . '</del> <ins>' . $active_price . $product->get_price_suffix() . '</ins>';
    } else {
        $price = $regular_price;
    }

    // When a default variation is set for the variable product
    if( get_default_variation( $product ) ) {
        $default_variaton = get_default_variation( $product );
        if( ! empty($default_variaton['price_html']) ){
            $price_html = $default_variaton['price_html'];
        } else {
            if ( ! $product->is_on_sale() )
                $price_html = $price = wc_price($default_variaton['display_price']);
            else
                $price_html = $price;
        }
        $availiability = $default_variaton['availability_html'];
    } else {
        $price_html = $price;
        $availiability = '';
    }
    // Update for woocommerce versions Up to 3.7
    $attribute_selector = version_compare( WC_VERSION, '3.7', '<' ) ? 'select' : 'input.variation_id';
    
    // Styles ?>
    <style>
        div.woocommerce-variation-price,
        div.woocommerce-variation-availability,
        div.hidden-variable-price {
            height: 0px !important;
            overflow:hidden;
            position:relative;
            line-height: 0px !important;
            font-size: 0% !important;
        }
    </style>
    <?php 

    echo '<p class="price">'.$price_html.'</p>
    <div class="wc-availability">'.$availiability.'</div>
    <div class="hidden-variable-price" >'.$price.'</div>';
}