<?php
namespace Elementor;

class Get_Variations_With_Filter extends Widget_Base {

	public function get_name() {
		return 'cew-get-variations-with-filter';
	}

	public function get_title() {
		return 'Product filter variations';
	}

	public function get_icon() {
		return 'eicon-products';
	}

	public function get_keywords() {
		return [ 'variation', 'variations', 'product' ];
	}

	public function get_categories() {
		return [ 'basic' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Content', 'elementor' ),
			]
		);

		$pquery = new \WP_Query( array(
			'post_type'      => 'product',
			'post_status'    => 'publish',
			'posts_per_page' => -1
		) );

		$title = [];
		$x = [];
		$id_parent = [];
		while ( $pquery->have_posts() ) : $pquery->the_post();

			$_product = wc_get_product(get_the_ID());
			if ($_product->is_type('variable'))
			{
				// Product has variations
				array_push($title, get_the_title());
				array_push($id_parent, get_the_ID());
			}
		endwhile;

		foreach ($title as $key => $val){
			$x[$val] = $val;
		}

		wp_reset_postdata();

		$this->add_control(
			'Product',
			[
				'label' => __( 'Product', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT2,
				'placeholder' => __( 'Select your product', 'elementor' ),
				'multiple' => false,
				'options' => $x,
			]
		);

		$mat = [];
		$matx = [];
		$terms = get_terms([
			'taxonomy' => 'pa_material',
			'hide_empty' => false,
		]);

		foreach ($terms as $term) {
			array_push($mat, $term->slug);
		}

		foreach ($mat as $key => $val){
			$matx[$val] = $val;
		}

		$this->add_control(
			'Variation Material',
			[
				'label' => __( 'Variation Material', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT2,
				'placeholder' => __( 'Select your material', 'elementor' ),
				'multiple' => false,
				'options' => $matx,
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		?>

		<style>
		.product-variation h6 {
			font-family: 'Montserrat';
			color: #005b75;
			font-size: 13px;
			font-weight: bold;
			text-transform: uppercase;
			margin-bottom: 15px;
		}

		.variation-product {
			font-family: 'Montserrat';
			color: #005b75;
			font-size: 18px;
			font-weight: bold;
		}

		.variation-attr h4 {
			font-family: 'Montserrat';
			border-bottom: none;
			display: inline-block;
			font-weight: 500;
			color: #005b75;
			font-size: 24px;
			line-height: 30px;
			margin-bottom: 15px!important;
			min-height: 52px;
		}
		
		.product-variation .fa {
			color: #7dd4c4;
			padding-right: 5px;
			font-size: 18px;
			padding-bottom: 5px;
		}

		</style>

		<?php
		$settings       = $this->get_settings_for_display();
		$product_title  = $settings['Product'];
		$var            = $settings['Variation Material'];

			// Define Query Arguments
			$has_variable_ids = [];
			$args_parent = new \WP_Query( array(
				'post_type'      => 'product',
				'post_status'    => 'publish',
				'title'          => $product_title
			) );
				foreach ($args_parent->posts as $post)
				{
					$_product = wc_get_product($post->ID);

					if ($_product->is_type('variable'))
					{
                        $has_variable_ids[] = $post->ID;
					}
				}

			$args = [
				'post_type' => ['product_variation'],
				'orderby' => 'meta_value_num',
				'order' => 'DESC',
				'post_status' => 'publish',
				'post_parent__in' => $has_variable_ids,
				'taxonomy'       => $var,
			];

			$var_query = new \WP_Query($args);

			//test html
			if ($var_query->have_posts()) : ?>
			 <div class="container product-variation">
				<div class="row">
				<?php
				$args = array(
					'taxonomy'               => 'pa_material',
					'orderby'                => 'name',
					'order'                  => 'ASC',
					'hide_empty'             => false,
					'slug'                   => [$var],
				);
				$the_query = new \WP_Term_Query($args);

				foreach($the_query->get_terms() as $term) { ?>
                        <div class="col-md-4">
                            <div class="katoon-content">
                                <img src="<?php echo get_field('material_image',$term); ?>">
                                <h2><?= $term->name; ?></h2>
                                <p><?= $term->description ;?></p>
                            </div>
                        </div>
                    <?php
				}

				//get data review
                $revs = [];
				$queryOpts = array(
					'nopaging' => true,
					'post_type' => 'wpcr3_review',
					'post_status' => 'publish'
				);
				$posts = new \WP_Query($queryOpts);

				foreach ($posts->posts as $key => $post) {
					$rvRating   = get_post_meta($post->ID, 'wpcr3_review_rating' ,true);
					$reviewed_post_id = get_post_meta($post->ID, 'wpcr3_review_post', true);
					$rev = [
					    'id' => $reviewed_post_id,
                        'rate'  => $rvRating
                    ];
					array_push($revs, $rev);
				}
				//end get data review
				
				while ($var_query->have_posts()) : $var_query->the_post();
					$product = wc_get_product(get_the_ID());

					$variation_variations = $product->get_variation_attributes();
					$star = 0;
					$c = 0;
					if($variation_variations['attribute_pa_material'] == $var){
						foreach ($revs as $key => $rev) {
							if($rev['id'] == $var_query->post->post_parent){
							    $c++;
							    $star += (int)$rev['rate'];
                            }
						}
						if($c !== 0){
						    $stars = $star / $c;
                        }else{
						    $stars = 5;
                        }
						?>
                        <div class="col-md-4">
                            <a href="<?php echo esc_url(the_permalink()); ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID()); ?>" >
								<?php
								$terms = wp_get_post_terms( $var_query->post->post_parent, 'product_cat' );
								foreach ( $terms as $term ) {
									$cats_name = $term->name;
									break;
								}
								?>
                                <h6 class="variation-title"> <?= $cats_name; ?> </h6>
								<?php
								if($stars !== 0 && $stars !== null){
									for ($i = 1; $i <= $stars; $i++){
										?>
                                        <i class="fa fa-star"></i>
										<?php
									}
								} else {
									?>
                                    <div class="star-ratings">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
									<?php
								}
								?>

                                <div class="variation-attr">
                                    <h4>
										<?php isset($variation_variations['attribute_pa_color']) ? _e($variation_variations['attribute_pa_color']) : "" ;
										isset($variation_variations['attribute_pa_material']) ? _e('/'.$variation_variations['attribute_pa_material']) : "";
										isset($variation_variations['attribute_pa_weight']) ? _e($variation_variations['attribute_pa_weight']) : "";
										?>
                                    </h4>

                                </div>
                                <div class="variation-product">
									<?= $product->get_price_html() ?>
                                </div>
                            </a>
                        </div>
						<?php
                    }
				endwhile;
				?>
                </div>
            </div>

				<?php
			endif;
			wp_reset_postdata();


	}

	protected function _content_template() {

	}


}