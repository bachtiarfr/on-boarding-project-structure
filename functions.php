<?php
/**
 * cones functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cones
 */

if ( ! function_exists( 'cones_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cones_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cones, use a find and replace
		 * to change 'cones' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cones', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'cones' )
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'cones_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );
        		
// 		remove_theme_support( 'wc-product-gallery-zoom' );
	}
endif;
add_action( 'after_setup_theme', 'cones_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cones_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'cones_content_width', 640 );
}
add_action( 'after_setup_theme', 'cones_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cones_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'cones' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cones' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'cones_widgets_init' );

/**
 * Include scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Bootstrap Walker
 */
require get_template_directory() . '/inc/bootstrap_walker.php';

/**
 * BRegister CPT example : edamame
 */
// require get_template_directory() . '/inc/cpt-edamame.php';

/**
 * Register ACF options
 */
require get_template_directory() . '/inc/acf-options.php';

/**
 * Template functions ACF 
 */
require get_template_directory() . '/inc/template-acf-functions.php';

/**
 * Register Custom Elementor Widget - CEW
 */
require get_template_directory() . '/inc/custom-elementor-widget/register-cew.php';

/**
 * Register Custom Email Review
 */
require get_template_directory() . '/inc/custom-email/custom-email-review.php';

/**
 * Register Custom Email Review
 */
require get_template_directory() . '/inc/custom-email/custom-email-templates.php';

/**
 * Redirections
 */
require get_template_directory() . '/inc/redirections.php';

/**
 * Customize product data tabs
 */
//add_filter( 'woocommerce_product_tabs', 'woo_custom_reviews_tab', 98 );
//function woo_custom_reviews_tab( $tabs ) {
//
//	$tabs['reviews']['callback'] = 'woo_custom_reviews_tab_content';	// Custom description callback
//
//	return $tabs;
//}
//
//function woo_custom_reviews_tab_content() {
//    $ID = get_the_ID();
//	echo '<h2>Recensie</h2>';
//	echo do_shortcode('[WPCR_SHOW POSTID="23" NUM="5" PAGINATE="1" PERPAGE="5" SHOWFORM="1" HIDEREVIEWS="0" HIDERESPONSE="0" SNIPPET="" MORE="" HIDECUSTOM="0" ]');
//}

//add_action('template_redirect','check_if_logged_in');
//function check_if_logged_in()
//{
//	$pageid = wc_get_page_id( 'checkout' ); // your checkout page id
//	if(!is_user_logged_in() && is_page($pageid))
//	{
//		$url = add_query_arg(
//			'redirect_to',
//			get_permalink($pageid),
//			get_permalink(324)
//		);
//		wp_redirect($url);
//		exit;
//	}
//}
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter' );
function wcc_change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = '<i class="fa fa-angle-right"></i>';
	return $defaults;
}

add_filter( 'woocommerce_cart_needs_shipping_address', '__return_false');

// add the action 
add_action( 'woocommerce_after_checkout_form', 'action_woocommerce_after_checkout_form', 10, 1 ); 
// remove the action 
remove_action( 'woocommerce_after_checkout_form', 'action_woocommerce_after_checkout_form', 10, 1 );

// show product image on review order checkout page
add_filter( 'woocommerce_cart_item_name', 'ts_product_image_on_checkout', 10, 3 );
 
function ts_product_image_on_checkout( $name, $cart_item, $cart_item_key ) {
     
    /* Return if not checkout page */
    if ( ! is_checkout() ) {
        return $name;
    }
     
    /* Get product object */
    $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
 
    /* Get product thumbnail */
    $thumbnail = $_product->get_image();
 
    /* Add wrapper to image and add some css */
    $image = '<div class="ts-product-image" style="width: 52px; height: 45px; display: inline-block; padding-right: 7px; vertical-align: middle;">'
                . $thumbnail .
            '</div>'; 
 
    /* Prepend image to name and return it */
    return $image . $name;
}

//auto login afer register
// function auto_login_new_user( $user_id ) {
// 	wp_set_current_user($user_id);
// 	wp_set_auth_cookie($user_id);
    
// 	$cc = WC()->cart->get_cart_contents_count();
// 	if ($cc > 0){
// 		wp_redirect( wc_get_checkout_url() );
// 	}else{
// 		wp_redirect( home_url() );
// 	}
// 	exit();
// }
// add_action( 'user_register', 'auto_login_new_user' );

/*
 * Auto Login After Email Confirmation. Tags auto login, email confirmation
 */

add_action( 'wppb_activate_user', 'wppb_custom_autologin_redirect', 10, 3 );
function wppb_custom_autologin_redirect( $user_id, $password, $meta ){
	// hack to fix conflict with WP Voting Contest in C:\www\pb20\wp-content\plugins\wp-voting-contest\includes\votes-save.php lines 420, 421, 422
	// basically WP Voting Contenst will login any email field that's submitted via POST. So we're overwriting the global $current_user for this particular instance.
	global $current_user;
	$current_user = 0;

	$token = wppb_create_onetime_token( 'pb_autologin_'.$user_id, $user_id );

	$cc = WC()->cart->get_cart_contents_count();
	if ($cc > 0){
		$location = add_query_arg( array(
			'pb_autologin' => 'true',
			'pb_uid'       => $user_id,
			'pb_token'     => $token,
		), wc_get_checkout_url());
	}else{
		$location = add_query_arg( array(
			'pb_autologin' => 'true',
			'pb_uid'       => $user_id,
			'pb_token'     => $token,
		), home_url());
	}

	echo "<script> window.location.replace('$location'); </script>";
}

add_action( 'init', 'wppb_custom_autologin' );
function wppb_custom_autologin(){
	if( isset( $_GET['pb_autologin'] ) && isset( $_GET['pb_uid'] ) &&  isset( $_GET['pb_token'] )  ){
		$uid = $_GET['pb_uid'];
		$token  = $_GET['pb_token'];
		require_once( ABSPATH . 'wp-includes/class-phpass.php');
		$wp_hasher = new PasswordHash(8, TRUE);
		$time = time();

		$hash_meta = get_user_meta( $uid, 'pb_autologin_' . $uid, true);
		$hash_meta_expiration = get_user_meta( $uid, 'pb_autologin_' . $uid . '_expiration', true);

		if ( ! $wp_hasher->CheckPassword($token . $hash_meta_expiration, $hash_meta) || $hash_meta_expiration < $time  ){
			//wp_redirect( $current_page_url . '?wpa_error_token=true' );
			die (' Dat mag u niet doen. ');
			exit;
		} else {
			wp_set_auth_cookie( $uid );
			delete_user_meta($uid, 'pb_autologin' . $uid );
			delete_user_meta($uid, 'pb_autologin' . $uid . '_expiration');

			$cc = WC()->cart->get_cart_contents_count();
			if ($cc > 0){
				wp_redirect( wc_get_checkout_url() );
			}else{
				// wp_redirect( home_url() );
				wp_redirect( site_url( 'login' ) );
			}
			exit;
		}
	}
}

function wppb_create_onetime_token( $action = -1, $user_id = 0 ) {
	$time = time();

	// random salt
	$key = wp_generate_password( 20, false );

	require_once( ABSPATH . 'wp-includes/class-phpass.php');
	$wp_hasher = new PasswordHash(8, TRUE);
	$string = $key . $action . $time;

	// we're sending this to the user
	$token  = wp_hash( $string );
	$expiration = $time + 60*10;
	$expiration_action = $action . '_expiration';

	// we're storing a combination of token and expiration
	$stored_hash = $wp_hasher->HashPassword( $token . $expiration );

	update_user_meta( $user_id, $action , $stored_hash ); // adjust the lifetime of the token. Currently 10 min.
	update_user_meta( $user_id, $expiration_action , $expiration );
	return $token;
}