<div id="header-halfpage" class="container-fluid" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
    <div class="overlay-black"></div>
    
    <div class="container d-flex align-items-center justify-content-start">
        <h1><?php the_title(); ?></h1>
    </div>
</div>