<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package YourThemeName
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> id="html">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>

<div id="page" class="site">

	<!-- Contact Navigation -->
	<nav id="contact-navigation" class="container-fluid">
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="contact-wrapper col-md-auto d-flex flex-wrap">
					<div class="telephone d-flex flex-wrap">
						<div class="item">
							<a href="tel:11223344">
								<img src="<?php echo get_template_directory_uri(); ?>/src/img/icons/call.svg">
								<span>11223344</span>
							</a>
						</div>
					</div>

					<span class="divider">|</span>
					
					
					<div class="mail">
						<a href="mailto:info@madeindonesia.com>">
							<img src="<?php echo get_template_directory_uri(); ?>/src/img/icons/mail.svg">
							<span>info@madeindonesia.com</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</nav>

	<!-- Main Navigation -->
	<header id="masthead" class="container-fluid">
		<div class="container">
			<div class="menu-wrapper">
				<div class="site-branding">
					<?php if (has_custom_logo()) {the_custom_logo();} else {?>
						<a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name');?></a>
					<?php }?>
				</div><!-- .site-branding -->

				<div class="hamburger hamburger--collapse">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>

			<nav id="site-navigation" class="main-navigation">
				<?php
				wp_nav_menu(array(
					'theme_location' => 'primary',
					'depth' => 2,
					'container' => false,
					'menu_class' => 'nav',
					'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
					'walker' => new Bootstrap_Walker_Nav_Menu(),
				));
				?>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->