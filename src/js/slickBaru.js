import $, { htmlPrefilter, ajax } from "jquery";
window.$ = window.jQuery = $;
import "bootstrap/dist/js/bootstrap";
import "slick-carousel/slick/slick"

(function ($) {
    
	$('.slickBaru').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		responsive: [
			{
				breakpoint: 9999,
				settings: "unslick"
			},
			{
				breakpoint: 767,
				 settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true,
						dots: true
					}
			}
		]
		// autoplay: true,
		// autoplaySpeed: 2000
	});

})(jQuery);
