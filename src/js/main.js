import $, { htmlPrefilter, ajax } from "jquery";
window.$ = window.jQuery = $;
import "bootstrap/dist/js/bootstrap";
import "slick-carousel/slick/slick"

(function ($) {
// 	console.log('slick lama');
	$("#relatedProductModal").modal("show");
	
	$('.review-content').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 2000
	});

    var img = $('<img src="https://cdn.shopify.com/s/files/1/0427/5247/5301/files/cart.png?v=1596007495" style="min-height: auto; width: 100% !important"><span>+</span>');
   $('body').find('a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart').html(img).end()
  	$('body').find('a.button.product_type_simple').html(img).end()
  	$('body').find('.product.woocommerce add_to_cart_inline a.button.product_type_variable.add_to_cart_button').html(img).end()
    $('body').find('a.button.product_type_variation.add_to_cart_button.ajax_add_to_cart').html(img).end()
  	$('body').find('a.button.product_type_simple').html(img).end()
  
    $('a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart').css("display", "inline-flex")
    $('a.button.product_type_simple').css("display", "inline-flex")
    $('a.button.product_type_variable.add_to_cart_button').css("display", "inline-flex")
  	$('a.button.product_type_variation.add_to_cart_button.ajax_add_to_cart').css("display", "inline-flex")

	//accordion click
	$('.card-header').click(function() { 
		$(this).find('i').toggleClass('fa fa-angle-up fa fa-angle-down'); 
	});  

})(jQuery);
